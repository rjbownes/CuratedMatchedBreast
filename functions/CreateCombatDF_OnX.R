CreateCombatDF_OnX <- function(DF_inp, annot){
  library(sva, help, pos = 2, lib.loc = NULL)
  
  annot <- annot[which(annot[,2] != "NA"), ]
  DF_inp <- DF_inp[, names(DF_inp) %in% annot[,1]]
  annot <- annot[which(annot[,1] %in% names(DF_inp)), ]
  
  x <- c(seq(levels(as.factor(as.character(annot[,2])))))[annot[,2]]
  y <- model.matrix(~1, data=DF_inp[1:ncol(DF_inp), ])
  z <- ComBat(dat = as.matrix(DF_inp), batch = x, mod = y, par.prior = TRUE, prior.plots = FALSE) %>% as.data.frame()
  z
}